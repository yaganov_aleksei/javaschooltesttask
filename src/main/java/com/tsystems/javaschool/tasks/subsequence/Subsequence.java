package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        // two lists must contain data
        if ((x == null) || (y == null)) throw new IllegalArgumentException();
        else {
            if (x.isEmpty()) return true;
            if (y.isEmpty()) return false;

            int currentIndex = 0;
            boolean flag = false;  //this flag will manage final result
            //for each element of X try to find the same in Y
            for (int i = 0; i < x.size(); i++) {
                //if current element from do not contain the same in Y-return false
                if (!y.contains(x.get(i))) {
                    flag = false;
                    break;
                } else {
                    //if programm will find element of X in Y - store current index of Y
                    // the next iteration start finding nex element of X in Y since stored index
                    int j = currentIndex;
                    while (j < y.size()) {

                        if (x.get(i) != y.get(j)) {
                            j++;
                            if (flag) flag = false;
                        } else {
                            flag = true;
                            currentIndex = j;
                            break;
                        }
                    }
                    // if Y list had finnished and programm did not find element from X-return false
                    if ((j == y.size()) && (i != x.size())) {
                        flag = false;
                        break;
                    }
                }
            }
            return flag;
        }

    }
}
