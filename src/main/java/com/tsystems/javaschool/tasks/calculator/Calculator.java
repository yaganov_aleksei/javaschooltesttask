package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        Calculate calc = new Calculate();
        Double result =  calc.parse(statement);
        if (result == null || result.isInfinite()) return null;
        else return calc.convertToString(result);
    }
}

class Calculate {
    int pos = -1;
    int currentChar;
    public String str;

    //convert result number to String, round to 4 digits after dot, remove if 0 at the end
    String convertToString(Double input) {
        String inputString = BigDecimal.valueOf(input).
                setScale(4, RoundingMode.HALF_UP).toString();
        for (int i = inputString.length() - 1; inputString.charAt(i) != '.'; i--) {
            if (inputString.charAt(i) != '0' ) break;
            inputString = inputString.substring(0,i);
        }
        //If the last symbol in string is dot-delete it
        if (inputString.charAt(inputString.length() - 1) == '.') {
            inputString = inputString.substring(0,inputString.length() - 1);

        }

        return inputString;
    }

    //Scan next symbol from input string
    void nextSymb() {
        currentChar = (++pos < str.length()) ? str.charAt(pos) : -1;
    }

    // check if the current symbol is operator(+,-,/,*,),( )
    boolean isOperator(int symb) {
        while (currentChar == ' ') nextSymb();
        if (currentChar == symb) {
            nextSymb();
            return true;
        }
        return false;
    }


    public Double parse(String str) {
        if (str == null) return null;
        this.str = str;
        nextSymb();
        Double x = addition();
        if (pos < str.length()) return null;
        return x;
    }

    //Make addition or subtract on numbers
    public Double addition() {
        Double res = multiplication();
        if (res == null || Double.isInfinite(res)) return null;
        while (true) {
            if (isOperator('+')) {
                Double temp;
                if ((temp = multiplication()) != null && !Double.isInfinite(temp)) {
                    res += temp;
                } else return null;
            }
            else if (isOperator('-')) {
                Double temp;
                if ((temp = multiplication()) != null && !Double.isInfinite(temp)) {
                    res -= temp;
                } else return null;
            }
            else return res;
        }
    }

    //Multiplicate or divide numbers
    public Double multiplication() {
        Double res = parseNumbers();
        while (true) {
            if (isOperator('*')) {
                Double temp;
                if ((temp = parseNumbers()) != null && !Double.isInfinite(temp)) {
                    res *= temp;
                } else return null;
            }
            else if (isOperator('/')) {
                Double temp;
                if ((temp = parseNumbers()) != null && !Double.isInfinite(temp)) {
                    res /= temp;
                } else return null;
            }
            else return res;
        }
    }

    //Method for parsing less prioritized symbols, such as numbers and parenthesis
    public Double parseNumbers() {
        Double num;
        int startPos = this.pos;
        if (isOperator('(')) {
            num = addition();
            if (!isOperator(')')) return null;
        } else if ((currentChar >= '0' && currentChar <= '9') || currentChar == '.') { // numbers
            while ((currentChar >= '0' && currentChar <= '9') || currentChar == '.') {
                nextSymb();
            }
            try {
                num = Double.parseDouble(str.substring(startPos, this.pos));
            } catch (NumberFormatException e) {
                num = null;
            }
        } else {
            return null;
        }
        return num;
    }

}
