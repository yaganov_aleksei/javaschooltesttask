package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    public static int numColumns; //number of columns in "pyramid"
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (ifPossible(inputNumbers)) {
            Collections.sort(inputNumbers);  //sorting numbers
            List<ArrayList<Integer>> inputArray = makeRowArrays(inputNumbers);
            for (ArrayList<Integer> row : inputArray) {
                makeRectangle(row, numColumns);
            }
            return(makeMatrix(inputArray));
        }
        else throw new CannotBuildPyramidException();
    }

    // Test if we can make Pyramid with input count numbers
    public static boolean ifPossible(List<Integer> list) {
        if (list.contains(null)) return false;
        int numCount = list.size();
        for (int numPerRow = 1; ; numPerRow++) {
            numCount -= numPerRow;
            if (numCount == 0) return true;
            if (numCount < 0) return false;
        }
    }

    // this function make arrays as rows of our pyramid (without external rows)
    public static List<ArrayList<Integer>> makeRowArrays(List<Integer> list){
        int iterSymbol = 0;
        List<ArrayList<Integer>> listPyramid = new ArrayList<>();
        for (int i = 1; iterSymbol < list.size() ; i++) {
            listPyramid.add(new ArrayList<>());
            for (int j = 0; j < i; j++) {
                listPyramid.get(i - 1).add(list.get(iterSymbol));
                if ((j + 1) != i)listPyramid.get(i - 1).add(0);
                iterSymbol++;
            }
            numColumns = listPyramid.get(i - 1).size();
        }
        return listPyramid;
    }

    // add external zero points to make rectangle
    public static void makeRectangle(ArrayList<Integer> row, int numColumns) {
        int zerosToAdd = numColumns - row.size();
        for (int i = 0; i < zerosToAdd / 2; i++) {
            row.add(0, 0);  //before numbers
            row.add(0);    // after numbers
        }
    }

    // create output 2d-array
    public static int[][] makeMatrix(List<ArrayList<Integer>> listPyramid) {
        int [][] matrix = new int[listPyramid.size()] [listPyramid.get(0).size()];
        for (int i = 0; i < listPyramid.size(); i++) {
            for(int j = 0; j < listPyramid.get(i).size(); j++) {
                matrix[i][j] = listPyramid.get(i).get(j);
            }
        }
        return matrix;
    }


}
